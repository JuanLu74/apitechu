package com.techu.apitechu.Controllers;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    // /apitechu/v1/products en el postman

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }
        }
        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es " + newProduct.getId());
        System.out.println("La descripción del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id en parámetro de la url es " + id);
        System.out.println("La id del producto a actualizar " + product.getId());
        System.out.println("La descripción del producto a actualizar " + product.getDesc());
        System.out.println("El precio del producto a actualizar " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Procuto encontrado");
                foundProduct = true;
                result = product;
            }
        }

        if (foundProduct == true) {
            System.out.println("Borrando procucto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }

    // Mini práctica: Patch
    // La id no se puede actualizar. La descripción y el precio sí se pueden actualizar.
    // El patch puede recibir un body que tenga descripción, o precio, o los dos. Debe funcionar en todos los casos.

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id) {
        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar parcialmente es " + id);
        System.out.println("La id del producto a actualizar " + productData.getId());
        System.out.println("La descripción del producto a actualizar " + productData.getDesc());
        System.out.println("El precio del producto a actualizar " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                result = productInList;

                if (productData.getDesc() != null) {
                    productInList.setDesc(productData.getDesc());
                    System.out.println("Se actualiza la descripción del producto");
                }

                if (productData.getPrice() > 0) {
                    productInList.setPrice(productData.getPrice());
                    System.out.println("Se actualiza el precio del producto");
                }

            }
        }
        return result;
    }

}